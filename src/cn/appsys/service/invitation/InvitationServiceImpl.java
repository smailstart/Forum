package cn.appsys.service.invitation;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.appsys.dao.invitation.InvitationMapper;
import cn.appsys.pojo.Page;
import cn.appsys.pojo.invitation;
import cn.appsys.pojo.reply_detai;

@Service
public class InvitationServiceImpl implements InvitationService{

	@Resource	
	private InvitationMapper InMapper;
	
	@Override
	public List<invitation> selectAll() {
		// TODO Auto-generated method stub
		return InMapper.selectAll();
	}

	//@Override
	public List<invitation> invitationListBytitle(String title) {
		// TODO Auto-generated method stub
		return InMapper.invitationListBytitle(title);
	}

	@Override
	public List<reply_detai> detailInfo(Integer id) {
		// TODO Auto-generated method stub
		return InMapper.detailInfo(id);
	}

	//@Override
	public int insertDetail(reply_detai det) {
		// TODO Auto-generated method stub
		return InMapper.insertDetail(det);
	}

	//@Override
	public int delete(Integer id) {
		// TODO Auto-generated method stub
		return InMapper.delete(id);
	}

}
