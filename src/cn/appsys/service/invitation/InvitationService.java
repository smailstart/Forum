package cn.appsys.service.invitation;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.appsys.pojo.Page;
import cn.appsys.pojo.invitation;
import cn.appsys.pojo.reply_detai;

public interface InvitationService {
	
	/*查询所有*/
	List<invitation> selectAll();	
	//模糊查询 
	List<invitation> invitationListBytitle(@Param("title")String title);
	//根据id查询回复
	List<reply_detai> detailInfo (@Param("id")Integer id);
	//添加回复
	int insertDetail(reply_detai det);
	
	int delete(@Param("id")Integer id); 
}
