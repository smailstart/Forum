package cn.appsys.pojo;

import java.util.Date;

public class reply_detai {
	private int id;//id
	private int invid;//vid
	private String content;//�ظ�����
	private String author;//�ǳ�
	private Date createdate;//ʱ��
	private int delStatus;//ɾ���ʶ
	
	public reply_detai(){
		
	}
	public reply_detai(int id, int invid, String content, String author,
			Date createdate,int delStatus) {
		super();
		this.id = id;
		this.invid = invid;
		this.content = content;
		this.author = author;
		this.createdate = createdate;
		this.delStatus = delStatus;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getInvid() {
		return invid;
	}
	public void setInvid(int invid) {
		this.invid = invid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public int getDelStatus() {
		return delStatus;
	}
	public void setDelStatus(int delStatus) {
		this.delStatus = delStatus;
	}
}
