package cn.appsys.dao.invitation;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.appsys.pojo.invitation;
import cn.appsys.pojo.reply_detai;

public interface InvitationMapper {

	
	List<invitation> selectAll();//查询所有
	
	List<invitation> invitationListBytitle(@Param("title")String title);//模糊查询 
	
	List<reply_detai> detailInfo (@Param("id")Integer id);//根据id查询回复
	
	int insertDetail(reply_detai det);//添加回复
	
	int delete(@Param("id")Integer id);
}
