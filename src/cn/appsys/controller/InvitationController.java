package cn.appsys.controller;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.jms.Session;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cn.appsys.pojo.Page;
import cn.appsys.pojo.invitation;
import cn.appsys.pojo.reply_detai;
import cn.appsys.service.invitation.InvitationService;

@Controller
@RequestMapping(value="/invit")
public class InvitationController {
	//logger
	private Logger logger = Logger.getLogger(InvitationController.class);	
	@Resource
	private InvitationService Invitation;
	
	
	
	/*查询所有*/
	@RequestMapping(value="/List.html",method=RequestMethod.GET)
	public String getInvitationList(Model model){
		List<invitation> list=null;
		list = Invitation.selectAll();
		model.addAttribute("InvitationList",list);
		return "index";
	}
	//模糊查询
	@RequestMapping(value="/ListBytitle.html",method=RequestMethod.POST)
	public String getInvitationListBytitle(Model model,String title){
		List<invitation> list=null;
		list = Invitation.invitationListBytitle(title);
		model.addAttribute("InvitationList",list);
		return "index";
	}
	//根据id查询回复
	@RequestMapping(value="/ListByid.html",method=RequestMethod.GET)
	public String getDetailListById(Model model,@RequestParam(value="id",required=false)Integer id,HttpSession sess){
		List<reply_detai> list=null;
		list = Invitation.detailInfo(id);
		sess.setAttribute("invid", id);
		model.addAttribute("detailList",list);
		return "detail";
	}
	//删除信息
	@RequestMapping(value="/delByid.html",method=RequestMethod.GET)
	public String delInfoById(@RequestParam(value="id",required=false)Integer id,Model model){
		int del = Invitation.delete(id);
		if(del==1){
			model.addAttribute("del","删除成功！");
		}else{
			model.addAttribute("del","删除失败！");
		}
		
		return "index";
		
		
	}	
	//添加回复
	@RequestMapping(value="/insert.html",method=RequestMethod.GET)
	public String insert(Model model,@RequestParam(value="invid",required=false)int invid){
		model.addAttribute("invid",invid);
		return "addDetail";
	}
	@RequestMapping(value="/insertDetail.html")
	public String insertDetail(Model model,@RequestParam(value="invid",required=false)int invid,reply_detai det){
		int result=0;
		det.setInvid(invid);
		det.setCreatedate(new Date());
		result = Invitation.insertDetail(det);
		return "index";
	}
	
	//返回
	@RequestMapping(value="/back.html")
	public String back(){
		return "index";
	}
}
