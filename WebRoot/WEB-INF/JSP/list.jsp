<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:if test="${InvitationList == null}">
	<jsp:forward page="/invit/getList" />
</c:if>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>学员列表</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>
<body>
	<div align="center">

		<table border="1" width="800" id="student">
			<tr>
				<td colspan="5" align="center"><h2>帖子列表</h2></td>
			</tr>
			<tr>
				<td colspan="5" align="center"></td>
			</tr>
			<tr>
				<td align="center">标题</td>
				<td align="center">内容摘要</td>
				<td align="center">作者</td>
				<td align="center">发布时间</td>
				<td align="center">操作</td>
			</tr>
			<c:forEach var="il" items="${InvitationList }">
				<tr>
					<td align="center">${il.title}</td>
					<td align="center">${il.summary}</td>
					<td align="center">${il.author}</td>
					<td align="center">${il.createdate}</td>
					<td align="center"><a href="toInfo">查看回复</a>&nbsp;||&nbsp;<a>删除</a></td>
				</tr>
			</c:forEach>

		</table>
		<div >
			页数[${page.currentPage}/${page.pageCount}]
			<c:if test="${page.currentPage>=1 }">
				<a href="#">首页</a>
				<a href="#">上一页</a>
			</c:if>
			<c:if test="${page.currentPage<page.pageCount}">
				<a href="#">下一页</a>
				<a href="#">末页</a>
			</c:if>
		</div>
	</div>
</body>
</html>
