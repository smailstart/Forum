<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:if test="${InvitationList == null}">
	<jsp:forward page="/invit/List.html" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div align="center">

		<table border="1" width="800" id="student">
			<tr>
				<td colspan="5" align="center"><h2>帖子列表</h2></td>
			</tr>
			<tr> 
				<td colspan="5" align="center">
					<form id="title" name="title"  action="${pageContext.request.contextPath }/invit//ListBytitle.html">
						帖子标题<input type="text" name="title" id="title"><input type="button" value="查询">
					</form>	
				</td>
			</tr>
			<tr>
				<td align="center">标题</td>
				<td align="center">内容摘要</td>
				<td align="center">作者</td>
				<td align="center">发布时间</td>
				<td align="center">操作</td>
			</tr>
			<c:forEach var="il" items="${InvitationList }">
				<tr>
					<td align="center">${il.title}</td>
					<td align="center">${il.summary}</td>
					<td align="center">${il.author}</td>
					<td align="center">${il.createdate}</td>
					<td align="center"><a href="${pageContext.request.contextPath }/invit/ListByid.html?id=${il.id}">查看回复</a>&nbsp;||&nbsp;<a href="${pageContext.request.contextPath }/invit/delByid.html?id=${il.id}">删除</a></td>
					
				</tr>
			</c:forEach>
				<tr>
					<td align="center" colspan="5">${del}</td>
				</tr>
		</table>

	</div>
</body>
</html>