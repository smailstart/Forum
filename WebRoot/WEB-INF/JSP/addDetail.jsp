<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="../statics/css/bootstrap.min.css">
</head>
<body>
<h1>添加回复</h1>
<div id="box">
	<form id="userForm" name="userForm"  action="${pageContext.request.contextPath }/invit/insertDetail.html?invid=${invid}">
	<div class="form-group">
    	<label for="exampleInputEmail1">回复内容</label>
    	<input type="text" class="form-control" id="content" name="content" placeholder="回复内容">
  	</div>
  	<div class="form-group">
   	 	<label for="exampleInputPassword1">回复昵称</label>
   		<input type="text" class="form-control" id="author" name="author"  placeholder=回复人昵称>
 	</div>
 	<button type="submit" class="btn btn-primary">提交</button>
 	<button type="reset" class="btn btn-primary">返回</button>
</form>
</div>
	<script type="text/javascript" src="../statics/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../statics/js/bootstrap.min.js"></script>
</body>
</html>