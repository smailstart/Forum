<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="../statics/css/bootstrap.min.css">
</head>
<body>
<div align="center">

		<table border="1" width="800" id="student">
			<tr>
				<td colspan="5" align="center"><h2>回复信息列表</h2></td>
			</tr>
			<tr>
				<td colspan="5" align="center"></td>
			</tr>
			<tr>
				<td align="center">回复内容</td>
				<td align="center">回复昵称</td>
				<td align="center">发布时间</td>
			</tr>
			<c:forEach items="${detailList}" var="item">
				<tr>
					<td align="center">${item.content }</td>
					<td align="center">${item.author }</td>
					<td align="center">${item.createdate }</td>
				</tr>
			</c:forEach>
				<tr>
					<td align="center" colspan="3" >
						<a href="${pageContext.request.contextPath }/invit/insert.html?invid=${invid}">添加回复</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="${pageContext.request.contextPath }/invit/back.html">返回</a>
					</td>
				</tr>
		</table>
</div>		
	<script type="text/javascript" src="../statics/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../statics/js/bootstrap.min.js"></script>
</body>
</html>